package com.project.challange.service;

import com.project.challange.model.DB_Challange;

import java.util.List;
import java.util.Map;

// Membuat service / CRUD
public interface Service {
//    Membuat method add
    DB_Challange registrasi(DB_Challange DBChallange);

//    Membuat method get by id
    DB_Challange getById(Long id);

//    Membuat method get all
    List<DB_Challange> getAll();

//    Membuat method update
    DB_Challange update(Long id, DB_Challange DBChallange);

//    Membuat method delete
    void delete(Long id);

//    Membuat method login
    Map<String, Object> login (DB_Challange loginUser);

}
