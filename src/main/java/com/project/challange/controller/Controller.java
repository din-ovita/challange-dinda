package com.project.challange.controller;

import com.project.challange.model.DB_Challange;
import com.project.challange.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// Membuat controller
@RestController

// Membuat request
@RequestMapping("/user")
public class Controller {
//    Memanggil service
    @Autowired
    private Service service;

//    Membuat request untuk menjalankan sistem add
    @PostMapping
    public DB_Challange registrasi(@RequestBody DB_Challange DBChallange) {
        return service.registrasi(DBChallange);
    }

//    Membuat request untuk menjalankan sistem get by id
    @GetMapping("/{id}")
    public DB_Challange getById(@PathVariable("id") Long id) {
        return service.getById(id);
    }

//    Membuat request untuk menjalankan sistem get all
    @GetMapping("/all")
    public List<DB_Challange> getAll() {
        return service.getAll();
    }

//    Membuat request untuk menjalankan sistem update
    @PutMapping("/{id}")
    public DB_Challange update(@PathVariable("id") Long id, @RequestBody DB_Challange DBChallange) {
        return service.update(id, DBChallange);
    }

//    Membuat request untuk menjalankan sistem delete
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {service.delete(id);}

//    Membuat request untuk menjalankan sistem login
    @PostMapping("/login")
    public Map<String, Object> login(@RequestBody DB_Challange loginUser) {
        return service.login(loginUser);
    }
}
