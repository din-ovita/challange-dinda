package com.project.challange.model;

import javax.persistence.*;

// Membuat tabel
@Entity
@Table(name = "user")
public class DB_Challange {
//    Membuat kolom "id"
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //membuat isi column tidak sama dengan column lainnya
    private Long id;

//    Membuat kolom "username"
    @Column(name = "username") //membuat column atau kolom dalam tabel
    private String username; //isi kolom

//    Membuat kolom "email"
    @Column(name = "email")
    private String email;

//    Membuat kolom "password"
    @Column(name = "password")
    private String password;

//    constructor kosong untuk mempermudah pemanggilan method model
    public DB_Challange() {
    }

//    constructor
    public DB_Challange(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

//    Getter dan Setter "id"
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //    Getter dan Setter "username"
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //    Getter dan Setter "email"
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    //    Getter dan Setter "password"
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
