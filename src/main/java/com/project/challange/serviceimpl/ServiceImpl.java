package com.project.challange.serviceimpl;

import com.project.challange.model.DB_Challange;
import com.project.challange.repository.Repository;
import com.project.challange.service.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Membuat service implements
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
//    Memanggil repository
    @Autowired
    private Repository repository;

//    Membuat sistem method add
    @Override
    public DB_Challange registrasi(DB_Challange DBChallange) {
        DB_Challange data = new DB_Challange();
        data.setUsername(DBChallange.getUsername());
        data.setEmail(DBChallange.getEmail());
        data.setPassword(DBChallange.getPassword());
        return repository.save(data);
    }

//    Membuat sistem method get Id
    @Override
    public DB_Challange getById(Long id) {
        return repository.findById(id).get();
    }

//    Membuat sistem method get all
    @Override
    public List<DB_Challange> getAll() {
        return repository.findAll();
    }

//    Membuat sistem method update by id
    @Override
    public DB_Challange update(Long id, DB_Challange DBChallange) {
        DB_Challange data = repository.findById(id).get();
        data.setUsername(DBChallange.getUsername());
        data.setEmail(DBChallange.getEmail());
        data.setPassword(DBChallange.getPassword());
        return repository.save(data);
    }

//    Membuat sistem method delete
    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

//    Membuat validasi login
    @Override
    public Map<String, Object> login(DB_Challange loginUser) {
        DB_Challange email = repository.findByEmail(loginUser.getEmail()).get();

        Map<String, Object> response = new HashMap<>();
        response.put("data", email);
        return response;
    }

}
