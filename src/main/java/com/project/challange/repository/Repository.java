package com.project.challange.repository;

import com.project.challange.model.DB_Challange;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

// Membuat repository untuk menyambungkan ke JPA dan melakukan query mysql
@org.springframework.stereotype.Repository
public interface Repository extends JpaRepository<DB_Challange, Long>{
    Optional<DB_Challange> findByEmail(String email);
}
